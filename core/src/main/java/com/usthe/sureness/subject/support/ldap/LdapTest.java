package com.usthe.sureness.subject.support.ldap;

import org.junit.Test;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.*;

/**
 * @author Ed
 * @create 2021-09-29 22:03
 */
public class LdapTest {
    private final String PROPERTIES_SCOPE = "ldap";

    private final String FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    private String url = "ldap://localhost:389";
    private String base = "1";
    private LdapContext ctx = null;
    private Control[] connCtls = null;
//    public static LdapContext ctx = null;
//    private final Control[] connCtls = null;

    @Test
    public void testUser() {
        connLDAP();
        List<User> list = getUser("users");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("ending");
        close();
    }
    @Test
    public void test() {
        connLDAP();
    }
    public boolean connLDAP() {

        Hashtable<String, String> env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, FACTORY);
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, "cn=admin,dc=ed,dc=org");
        env.put(Context.SECURITY_CREDENTIALS, "123");

        try {
            ctx = new InitialLdapContext(env, connCtls);
            System.out.println("认证成功！");
            return Boolean.TRUE;

        } catch (javax.naming.AuthenticationException e) {
            System.out.println("认证失败 ：");
            e.printStackTrace();
            return Boolean.FALSE;
        } catch (Exception e) {
            System.out.println("认证出错 ： ");
            e.printStackTrace();
            return Boolean.FALSE;
        } finally {
//            close();
        }
    }
    public void close() {
        if (ctx != null) {
            try {
                ctx.close();
                System.out.println("连接关闭");
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }
    public List<User> getUser(String uname) {
        List<User> users = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        SearchControls searchCtls = new SearchControls();
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        //过滤器，可改变查询条件
        String searchFilter = "(&(objectClass=inetOrgPerson))";
        String searchBase = "DC=ed,DC=org";
        //对象的每个属性名
        String[] returnedAtts = {"mail", "cn", "userPassword", "sn", "uid"};
        searchCtls.setReturningAttributes(returnedAtts);
        NamingEnumeration<SearchResult> answer;
        Map<String, String> mmap = new HashMap<>();
        try {
            answer = ctx.search(searchBase, searchFilter, searchCtls);
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                String name = sr.getName();
                User user = new User();
                if (name.contains("cn") || name.contains("uid")) {
                    String[] s = name.split(",");
                    String[] cns = s[0].split("=");
                    String cn = cns[cns.length - 1];
                    mmap.put(cn, sr.getNameInNamespace());
                    NamingEnumeration<? extends Attribute> attrs = sr.getAttributes().getAll();
                    while (attrs.hasMore()) {
                        Attribute attr = attrs.next();
//                        System.out.println(attr.getID());
                        switch (attr.getID()) {
                            case "cn":
                                user.setCn(attr.get().toString());
                                break;
                            case "sn":
                                user.setSn(attr.get().toString());
                                break;
                            case "mail":
                                user.setMail(attr.get().toString());
                                break;
                            case "uid":
                                user.setUid(attr.get().toString());
                                break;
                            case "userPassword":
                                user.setPassword(attr.get().toString());
                                break;
                        }
                    }
                    users.add(user);
                }
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
//        mmap.forEach((k, v) -> System.out.println("key : value = " + k + ":" + v));
        return users;
    }
}

