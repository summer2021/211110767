package com.usthe.sureness.subject.support.ldap;

/**
 * @author Ed
 * @create 2021-09-29 22:04
 */
public class User {
    private String cn;
    private String sn;
    private String mail;
    private String uid;
    private String password;

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "cn='" + cn + '\'' +
                ", sn='" + sn + '\'' +
                ", mail='" + mail + '\'' +
                ", uid='" + uid + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
