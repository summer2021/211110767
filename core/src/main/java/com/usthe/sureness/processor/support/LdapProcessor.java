package com.usthe.sureness.processor.support;

import com.usthe.sureness.processor.BaseProcessor;
import com.usthe.sureness.processor.exception.*;
import com.usthe.sureness.provider.SurenessAccount;
import com.usthe.sureness.provider.SurenessAccountProvider;
import com.usthe.sureness.subject.Subject;
import com.usthe.sureness.subject.support.LdapSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Hashtable;

/**
 * @author Ed
 * @create 2021-08-15 11:31
 */
public class LdapProcessor extends BaseProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DigestProcessor.class);

    private static final String ADMIN_PRINCIPAL = "cn=admin,dc=ed,dc=org";
    private static final String ADMIN_CREDENTIALS = "123";
    private static final String URL = "ldap://localhost:389";
    private static final String SCOPE = "dc=ed,dc=org";

    private static final String HEX_LOOKUP = "0123456789abcdef";
    private static MessageDigest md5Digest;

    private SurenessAccountProvider accountProvider;


    @Override
    public boolean canSupportSubjectClass(Class<?> var) {
        return var == LdapSubject.class;
    }

    @Override
    public Class<?> getSupportSubjectClass() {
        return LdapSubject.class;
    }

    @Override
    public Subject authenticated(Subject var) throws SurenessAuthenticationException {
        if (var.getPrincipal() == null || var.getCredential() == null) {
            String authenticate = getAuthenticate();
            throw new NeedDigestInfoException("you should try once with ldap auth information", authenticate);
        }
        String appId = (String) var.getPrincipal();
        String password = (String) var.getCredential();
        LdapContext ctx = null;
        SurenessAccount account = accountProvider.loadAccount(appId);

        System.out.println("appId= " + appId + ", password= " + password);


        try {
            ctx = connectLdapAdmin(ADMIN_PRINCIPAL, ADMIN_CREDENTIALS, URL);

        } catch (Exception e) {
            e.printStackTrace();
            // try catch 三次 第一次是ldap服务器连接， 第二次是通过filter搜索用户名， 第三次是验证
            throw new DisabledAccountException("failed to connect to server");
        }

        try {
            String filter = "(&(objectClass=*)(cn=" + appId + "))";
            String[] attrPersonArray = {"mail", "cn", "userPassword", "sn", "uid"};
            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(2);
            searchControls.setReturningAttributes(attrPersonArray);
            NamingEnumeration<SearchResult> answer = ctx.search(SCOPE, filter, searchControls);

            if(!answer.hasMore()){
                // 说明没查到应该第二次throw exception
                throw new IncorrectCredentialsException("incorrect username");
            }
        } catch (NamingException e) {
//            e.printStackTrace();
            throw new IncorrectCredentialsException("incorrect username");
        }

        // 密码应该加密
//        password = calcDigest(password);
        try {
            if(connectLdap(appId, password)){
                System.out.println("userName：" + appId + ", 登录成功");
            }else{
                throw new IncorrectCredentialsException("incorrect password");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            throw new IncorrectCredentialsException("incorrect password");
        }


        try {
            ctx.close();
        } catch (NamingException ex) {
            logger.info("--------->> 关闭LDAP连接失败");
        }

        // attention: need to set subject own roles from account
        var.setOwnRoles(account.getOwnRoles());
        return var;
    }

    private String getAuthenticate(){
        String nonce = calcDigest(String.valueOf(System.currentTimeMillis()));
        return "";
    }

    private String calcDigest(String first, String ... args){
        StringBuilder stringBuilder = new StringBuilder(first);
        if (args != null) {
            for (String str : args){
                stringBuilder.append(':').append(str);
            }
        }
        md5Digest.reset();
        md5Digest.update(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));
        return bytesToHexString(md5Digest.digest());
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(HEX_LOOKUP.charAt((aByte & 0xF0) >> 4));
            sb.append(HEX_LOOKUP.charAt((aByte & 0x0F)));
        }
        return sb.toString();
    }

    /**
     * 管理员连接
     *
     * @Description:
     * @param principle
     * @param password
     * @param url
     * @return
     */
    public LdapContext connectLdapAdmin(String principle, String password, String url) {
        LdapContext ctxTDS = null;
        Hashtable<String, Object> env = new Hashtable<String, Object>();
        env.put(Context.SECURITY_PRINCIPAL, principle);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.PROVIDER_URL, url);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        try {
            ctxTDS = new InitialLdapContext(env, null);
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return ctxTDS;
    }

    /**
     * ldap
     */
    public boolean connectLdap(String userName, String password) {
        Hashtable<String, String> env = new Hashtable<String, String>();
        boolean result = false;
        env.put(Context.SECURITY_PRINCIPAL, "cn=" + userName + "," + SCOPE);
        env.put(Context.SECURITY_CREDENTIALS, password);
        env.put(Context.PROVIDER_URL, URL);
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        try {
            new InitialLdapContext(env, null);
            result = true;
        } catch (Exception e) {
            System.out.println("userName：" + userName + ",SearchName:" + SCOPE + ", 登录失败");
        }
        return result;
    }

    public void setAccountProvider(SurenessAccountProvider accountProvider) {
        this.accountProvider = accountProvider;
    }
}
